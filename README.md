haskell-tracker - Haskell Package Version Tracker
=================================================

Here you will find the code for a web page that shows which versions,
if any, are available in Debian for Haskell packages uploaded to
[Hackage](https://hackage.haskell.org/).

A recent web page with the results may be available
[here](https://hackage.debian.net/).

Distribution field on Hackage
=============================

This tool can also [update](https://hackage.haskell.org/api#distro)
Debian's [package
map](https://hackage.haskell.org/distro/Debian/packages) on Hackage.

Spin up your own
================

Try this:

```
    cabal install hackage-tracker
    ./run
```

Questions or Problems?
======================

Please contact the [Haskell Team](https://wiki.debian.org/Teams/DebianHaskellGroup)
for additional needs. We look forward to your feedback. You are
helping the entire community. Thank you!
