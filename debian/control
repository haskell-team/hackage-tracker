Source: hackage-tracker
Section: devel
Priority: optional
Maintainer: Felix Lechner <felix.lechner@lease-up.com>
Build-Depends:
 cabal-install,
 debhelper-compat (= 10),
 dh-sequence-haskell,
 ghc,
 libghc-debian-dev,
 libghc-debian-prof,
 libghc-tar-dev,
 libghc-tar-prof,
 libghc-zlib-dev,
 libghc-zlib-prof,
Rules-Requires-Root: no
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/haskell-team/hackage-tracker.git
Vcs-Browser: https://salsa.debian.org/haskell-team/hackage-tracker
Homepage: https://hackage-tracker.debian.net/

Package: hackage-tracker
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: Haskell package version tracker
 Generates a file with information that can be used to update the Debian
 versions on Hackage for each package available there. Those versions appear
 in the Distribution field on Hackage.
 .
 Furthermore produces an HTML page that compares the Haskell package versions
 available in Debian with those available on Hackage and elsewhere.
 .
 The data is currently displayed at https://hackage-tracker.debian.net.
